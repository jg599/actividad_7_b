package com.example.germy.quickblox;

import android.os.Bundle;

import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose.mansilla on 08/02/2016.
 */
public class QB_ubicacion {
QB_ubicacion_listener listener ;


public QB_ubicacion( QB_ubicacion_listener listener){
    this.listener=listener;


}

  public void  coger_datos(){
      QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();


      QBCustomObjects.getObjects("localizacion", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
          @Override
          public void onSuccess(ArrayList<QBCustomObject> customObjects, Bundle params) {
        listener.get_loc(customObjects, params);
          }

          @Override
          public void onError(List<String> errors) {

          }
      });
    }

   public void coger_ubicacion(){

    }
}
