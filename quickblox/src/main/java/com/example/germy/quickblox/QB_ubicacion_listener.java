package com.example.germy.quickblox;

import android.os.Bundle;

import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;

/**
 * Created by jose.mansilla on 08/02/2016.
 */
public interface QB_ubicacion_listener {

    public void get_loc (ArrayList<QBCustomObject> customObjects, Bundle params);
}
